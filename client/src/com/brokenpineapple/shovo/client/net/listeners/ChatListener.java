package com.brokenpineapple.shovo.client.net.listeners;

import com.brokenpineapple.shovo.core.net.InputHandler;
import com.brokenpineapple.shovo.core.net.InputListener;
import com.brokenpineapple.shovo.core.net.packets.server.ChatPacket;

public class ChatListener extends InputListener {
    @InputHandler(ChatPacket.class)
    public void onChat(ChatPacket packet) {

    }
}
