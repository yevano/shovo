package com.brokenpineapple.shovo.client.net.listeners;

import com.brokenpineapple.shovo.client.Client;
import com.brokenpineapple.shovo.client.world.ClientWorld;
import com.brokenpineapple.shovo.core.net.InputHandler;
import com.brokenpineapple.shovo.core.net.InputListener;
import com.brokenpineapple.shovo.core.net.packets.server.ChunkPacket;
import com.brokenpineapple.shovo.core.util.StreamFactory;
import com.brokenpineapple.shovo.core.world.Chunk;

import java.io.IOException;

public class WorldListener extends InputListener {
    private ClientWorld world = Client.getInstance().getWorld();

    @InputHandler(ChunkPacket.class)
    public void onChunkLoaded(ChunkPacket packet) throws IOException {
        Chunk chunk = new Chunk(world, packet.getX(), packet.getY(), packet.getZ());
        chunk.read(StreamFactory.newDataByteInputStream(packet.getChunk()));
        world.getChunkMap().put(chunk);
    }


}
