package com.brokenpineapple.shovo.client.net;

import com.brokenpineapple.shovo.core.net.Packet;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

public class NetManager {
    private SocketChannel clientChannel;
    private Selector selector;

    private NetworkThread networkThread;

    public void open(String address, int port) throws IOException {
        clientChannel = SocketChannel.open(new InetSocketAddress(address, port));
        clientChannel.configureBlocking(false);

        selector = Selector.open();
        clientChannel.register(selector, SelectionKey.OP_READ);

        networkThread = new NetworkThread(selector);
        networkThread.start();
    }

    public void close() throws IOException {
        clientChannel.close();
    }

    public void sendPacket(Packet packet) throws IOException {
        ByteArrayOutputStream payloadStream = new ByteArrayOutputStream();
        packet.write(new DataOutputStream(payloadStream));
        byte[] payload = payloadStream.toByteArray();

        ByteArrayOutputStream headerByteStream = new ByteArrayOutputStream();
        DataOutputStream headerStream = new DataOutputStream(headerByteStream);
        headerStream.writeShort(packet.getID());
        headerStream.writeInt(payload.length);

        clientChannel.write(ByteBuffer.wrap(headerByteStream.toByteArray()));
        clientChannel.write(ByteBuffer.wrap(payload));
    }
}
