package com.brokenpineapple.shovo.client.net;

import com.brokenpineapple.shovo.client.Client;
import com.brokenpineapple.shovo.core.net.Dispatcher;
import com.brokenpineapple.shovo.core.net.InputQueue;
import com.brokenpineapple.shovo.core.net.Packet;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

public class NetworkThread implements Runnable {
    private Selector selector;

    private InputQueue inputQueue = new InputQueue();
    private int currentID;
    private int currentPayloadSize = -1;

    private boolean running = false;

    public NetworkThread(Selector selector) {
        this.selector = selector;
    }

    @Override
    public void run() {
        while(running) {
            try {
                if(selector.select() == 0) {
                    System.out.println("Nothing to select, stopping network thread");
                    this.stop();
                    break;
                }

                Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                SelectionKey key = iterator.next();

                SocketChannel clientChannel = (SocketChannel) key.channel();

                System.out.println(clientChannel.isBlocking());
                int len = inputQueue.fillFrom(clientChannel);
                System.out.println("Read " + len + " bytes");

                if (inputQueue.position() >= Packet.HEADER_BYTES && currentPayloadSize == -1) {
                    // If we have read the packet header, store it and remove it from the queue.
                    ByteBuffer packetHeader = inputQueue.peek(Packet.HEADER_BYTES);

                    currentID = packetHeader.getShort() & 0xffff;
                    currentPayloadSize = packetHeader.getInt();
                    inputQueue.dequeBytes(Packet.HEADER_BYTES);

                    System.out.println(currentID + ", " + currentPayloadSize);
                }

                if (currentPayloadSize <= inputQueue.position() && currentPayloadSize != -1) {
                    // If the entire payload has been read, send the packet to the Dispatcher
                    ByteBuffer packetPayload = inputQueue.peek(currentPayloadSize);
                    inputQueue.dequeBytes(currentPayloadSize);

                    Dispatcher.getInstance().dispatch(currentID, packetPayload.array(), clientChannel);
                    currentPayloadSize = -1;
                }
            } catch (IOException e) {
                try {
                    Client.getInstance().getNetManager().close();
                } catch(IOException innerException) {
                    innerException.printStackTrace();
                }
                this.stop();
                e.printStackTrace();
            }
        }
    }

    public void start() {
        running = true;
        new Thread(this).start();
    }

    public void stop() {
        running = false;
    }
}
