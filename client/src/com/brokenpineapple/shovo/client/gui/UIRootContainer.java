package com.brokenpineapple.shovo.client.gui;

public class UIRootContainer extends UIContainer {
    public UIRootContainer(int width, int height) {
        super();
        super.width = width;
        super.height = height;
        super.resizeBuffer(width, height);
    }
}
