package com.brokenpineapple.shovo.client.gui.components;

import com.brokenpineapple.shovo.client.gui.HorizontalAlignment;
import com.brokenpineapple.shovo.client.gui.UIComponent;
import com.brokenpineapple.shovo.client.gui.VerticalAlignment;

import java.awt.*;
import java.awt.event.MouseEvent;

public class UIButton extends UIComponent {
    public static final Font DEFAULT_FONT = new Font("", Font.PLAIN, 24);

    protected boolean depressed = false;

    protected String text;
    protected Font font;
    protected HorizontalAlignment horizontalAlignment;
    protected VerticalAlignment verticalAlignment;

    public UIButton(int x, int y, int width, int height, String text,
                    HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment) {
        super(
                x +
                ( horizontalAlignment == HorizontalAlignment.LEFT   ? 0
                        : horizontalAlignment == HorizontalAlignment.CENTER ? -width/2
                        : -width),
                y +
                ( verticalAlignment   == VerticalAlignment.UP       ? 0
                        : verticalAlignment   == VerticalAlignment.CENTER   ? -width/2
                        : -width), width, height);
        this.text = text;
        this.font = UIButton.DEFAULT_FONT;
        this.horizontalAlignment = horizontalAlignment;
        this.verticalAlignment = verticalAlignment;
    }

    public void draw(Graphics2D g) {
        int textWidth = g.getFontMetrics().stringWidth(text);
        int textHeight = g.getFontMetrics().getHeight();

        if (depressed) {
            g.setColor(new Color(126, 53, 9));
        } else {
            g.setColor(new Color(82, 36, 6));
        }

        g.fillRect(0, 0, width, height);

        g.setColor(new Color(37, 16, 4));
        g.drawRect(0, 0, width, height);

        g.setColor(new Color(243, 242, 161));
        g.setFont(font);
        g.drawString(text, (int)((((float)width)/2) - ((float)textWidth)/2), (int)((((float)height)/2) + ((float)textHeight)/2));
    }

    @Override
    public void onMouseDown(MouseEvent e) {
        if (isInside(e.getPoint()) && !depressed) {
            super.repaint();
            depressed = true;
        }
    }

    @Override
    public void onMouseUp(MouseEvent e) {
        if (depressed) {
            super.repaint();
            depressed = false;
        }
    }
}
