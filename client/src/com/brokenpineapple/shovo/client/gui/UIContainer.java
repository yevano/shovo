package com.brokenpineapple.shovo.client.gui;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Set;

public class UIContainer extends UIComponent {
    private BufferedImage buffer;
    protected Set<UIComponent> components = new HashSet<UIComponent>();

    private Set<UIComponent> addedComponents = new HashSet<UIComponent>();
    private Set<UIComponent> removedComponents = new HashSet<UIComponent>();

    int xOffset, yOffset;
    int widthOffset, heightOffset;

    public UIContainer() {
        buffer = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
    }

    public void resizeBuffer(int width, int height) {
        BufferedImage oldBuffer = buffer;
        this.buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        if(oldBuffer != null) {
            Graphics g = buffer.getGraphics();
            g.drawImage(oldBuffer, 0, 0, null);
            g.dispose();
        }
    }

    @Override
    public void update(int delta) {
        for(UIComponent addedComponent : addedComponents) {
            components.add(addedComponent);
        }

        for(UIComponent removedComponent : removedComponents) {
            components.remove(removedComponent);
        }

        for(UIComponent component : components) {
            component.update(delta);
        }
    }

    @Override
    public void draw(Graphics2D g) {
        if(repaint) {
            this.repaint = false;

            int maxWidth = 0, maxHeight = 0;

            for(UIComponent component : components) {
                Rectangle bounds = component.getDrawBounds();
                if(bounds.x + bounds.width > maxWidth) maxWidth = bounds.x + bounds.width;
                if(bounds.y + bounds.height > maxHeight) maxHeight = bounds.y + bounds.height;
            }

            if(maxWidth > buffer.getWidth() || maxHeight > buffer.getHeight()) {
                this.resizeBuffer(maxWidth, maxHeight);
            }

            Graphics2D bufferGraphics = (Graphics2D) buffer.getGraphics();
            bufferGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            for(UIComponent component : components) {
                if(component.shouldRepaint()) {
                    bufferGraphics.translate(component.x, component.y);
                    component.draw(bufferGraphics);
                    bufferGraphics.translate(-component.x, -component.y);
                }
            }

            bufferGraphics.dispose();

            g.drawImage(buffer, x + xOffset, y + yOffset, width - widthOffset - xOffset, height - heightOffset - yOffset, null);
        }
    }

    @Override
    public void onResize(int newWidth, int newHeight) {
        this.resizeBuffer(newWidth, newHeight);
    }

    @Override
    public void onMouseDown(MouseEvent e) {
        for(UIComponent component : components) {
            component.onMouseDown(e);
        }
    }

    @Override
    public void onMouseUp(MouseEvent e) {
        for(UIComponent component : components) {
            component.onMouseUp(e);
        }
    }

    @Override
    public void onMouseMove(MouseEvent e) {
        for(UIComponent component : components) {
            component.onMouseMove(e);
        }
    }

    @Override
    public void onMouseDrag(MouseEvent e) {
        for(UIComponent component : components) {
            component.onMouseDrag(e);
        }
    }

    @Override
    public void onKeyDown(KeyEvent e) {
        for(UIComponent component : components) {
            component.onKeyDown(e);
        }
    }

    @Override
    public void onKeyUp(KeyEvent e) {
        for(UIComponent component : components) {
            component.onKeyUp(e);
        }
    }

    public void addComponent(UIComponent component) {
        addedComponents.add(component);
        // TODO move this, cus this is unsafe?
        component.parent = this;
    }

    public void removeComponent(UIComponent component) {
        removedComponents.remove(component);
    }
}
