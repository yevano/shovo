package com.brokenpineapple.shovo.client.gui;

public enum VerticalAlignment {
    UP, CENTER, DOWN
}
