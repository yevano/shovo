package com.brokenpineapple.shovo.client.gui.views;

import com.brokenpineapple.shovo.client.gui.HorizontalAlignment;
import com.brokenpineapple.shovo.client.gui.UIContainer;
import com.brokenpineapple.shovo.client.gui.VerticalAlignment;
import com.brokenpineapple.shovo.client.gui.components.UIButton;

import java.awt.*;

public class UIMainMenu extends UIContainer {
    private UIButton playButton;

    public UIMainMenu() {
        playButton = new UIButton(20, 20, 200, 50, "Play", HorizontalAlignment.LEFT, VerticalAlignment.UP);
        super.addComponent(playButton);
    }

    @Override
    public void update(int delta) {
        super.update(delta);
        if(parent != null) {
            super.setSize(parent.getWidth(), parent.getHeight());
        }
    }

    @Override
    public void draw(Graphics2D g) {
        g.setColor(Color.GRAY);
        g.fillRect(x, y, width, height);
        super.draw(g);
    }
}
