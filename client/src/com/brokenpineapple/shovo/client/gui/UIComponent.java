package com.brokenpineapple.shovo.client.gui;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class UIComponent {
    protected UIContainer parent;

    protected int x, y;
    protected int width, height;

    protected boolean enabled;
    protected boolean visible;

    protected boolean repaint = true;

    public UIComponent() {

    }

    public UIComponent(int x, int y, int width, int height) {
        this(null, x, y, width, height);
    }

    public UIComponent(UIContainer parent, int x, int y, int width, int height) {
        this.parent = parent;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public void update(int delta) {

    }

    public void draw(Graphics2D g) {

    }

    public void onRelocate(int newX, int newY) {

    }

    public void onResize(int newWidth, int newHeight) {

    }

    public void repaint() {
        this.repaint = true;
        if(parent != null) {
            parent.repaint();
        }
    }

    public void repaintWithoutNotify() {
        this.repaint = true;
    }

    public boolean shouldRepaint() {
        return repaint;
    }

    public void onMouseDown(MouseEvent e) {

    }

    public void onMouseUp(MouseEvent e) {

    }

    public void onMouseMove(MouseEvent e) {

    }

    public void onMouseDrag(MouseEvent e) {

    }

    public void onKeyDown(KeyEvent e) {

    }

    public void onKeyUp(KeyEvent e) {

    }

    public boolean isInside(Point p) {
        int absX = this.getAbsoluteX();
        int absY = this.getAbsoluteY();
        int px = p.x;
        int py = p.y;
        return px >= absX && px <= absX + width && py >= absY && py <= absY + height;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, width, height);
    }

    public Rectangle getDrawBounds() {
        return this.getBounds();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        if(this.x != x) {
            this.onRelocate(x, y);
            this.x = x;
        }
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        if(this.y != y) {
            this.onRelocate(x, y);
            this.y = y;
        }
    }

    public int getAbsoluteX() {
        if(parent != null) {
            return parent.getAbsoluteX() + x;
        }
        return x;
    }

    public int getAbsoluteY() {
        if(parent != null) {
            return parent.getAbsoluteY() + y;
        }
        return y;
    }

    public void setLocation(int x, int y) {
        if(this.x != x || this.y != y) {
            this.onRelocate(x, y);
            this.x = x;
            this.y = y;
        }
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        if(this.width != width) {
            this.onResize(width, height);
            this.width = width;
        }
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        if(this.height != height) {
            this.onResize(width, height);
            this.height = height;
        }
    }

    public void setSize(int width, int height) {
        if(this.width != width || this.height != height) {
            this.onResize(width, height);
            this.width = width;
            this.height = height;
        }
    }
}
