package com.brokenpineapple.shovo.client.gui;

import com.brokenpineapple.shovo.client.gui.views.UIGame;
import com.brokenpineapple.shovo.client.gui.views.UIMainMenu;

public class Views {
    public static final UIContainer uiGame = new UIGame();
    public static final UIContainer uiMainMenu = new UIMainMenu();
}
