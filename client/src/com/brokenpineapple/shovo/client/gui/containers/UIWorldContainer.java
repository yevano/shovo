package com.brokenpineapple.shovo.client.gui.containers;

import com.brokenpineapple.shovo.client.gui.UIContainer;
import com.brokenpineapple.shovo.client.world.ChunkRenderer;

import java.awt.Graphics2D;

public class UIWorldContainer extends UIContainer {
    private ChunkRenderer chunkRenderer = new ChunkRenderer();

    @Override
    public void draw(Graphics2D g) {
        chunkRenderer.draw(g);
    }

    public ChunkRenderer getChunkRenderer() {
        return chunkRenderer;
    }
}
