package com.brokenpineapple.shovo.client.gui;

public enum HorizontalAlignment {
    LEFT, CENTER, RIGHT
}
