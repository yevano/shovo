package com.brokenpineapple.shovo.client.testbed;

import com.brokenpineapple.shovo.client.net.NetManager;
import com.brokenpineapple.shovo.core.net.Dispatcher;
import com.brokenpineapple.shovo.core.net.InputHandler;
import com.brokenpineapple.shovo.core.net.InputListener;
import com.brokenpineapple.shovo.core.net.packets.client.AuthPacket;
import com.brokenpineapple.shovo.core.net.packets.client.LocalChatPacket;
import com.brokenpineapple.shovo.core.net.packets.server.AuthResponsePacket;
import com.brokenpineapple.shovo.core.net.packets.server.ChatPacket;

import java.io.IOException;

public class ClientNetTest extends InputListener {
    public static void main(String[] args) throws Exception {
        new ClientNetTest();
    }

    private NetManager netManager = new NetManager();

    public ClientNetTest() throws IOException {
        this.netManager = new NetManager();
        netManager.open("localhost", 33433);
        System.out.println("Opened connection with server");
        Dispatcher.getInstance().registerListener(this);

        auth();
    }

    private void auth() throws IOException {
        netManager.sendPacket(new AuthPacket("Yevano", "lepassword"));
    }

    @InputHandler(ChatPacket.class)
    public void onChat(ChatPacket packet) {
        System.out.println("Chat: " + packet.getMessage());
    }

    @InputHandler(AuthResponsePacket.class)
    public void onAuthResponse(AuthResponsePacket packet) {
        System.out.println("YOYOYO");
        if (packet.isSuccessful()) {
            System.out.println("Successfully authed with the server!");
        }
    }
}
