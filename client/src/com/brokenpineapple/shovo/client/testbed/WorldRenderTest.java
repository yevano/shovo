package com.brokenpineapple.shovo.client.testbed;

import com.brokenpineapple.shovo.client.GameApplet;
import com.brokenpineapple.shovo.client.gui.UIContainer;
import com.brokenpineapple.shovo.client.gui.UIRootContainer;
import com.brokenpineapple.shovo.client.world.ChunkRenderer;
import com.brokenpineapple.shovo.client.world.ClientWorld;
import com.brokenpineapple.shovo.core.world.Chunk;
import com.brokenpineapple.shovo.core.world.Tile;
import com.brokenpineapple.shovo.core.world.World;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;

public class WorldRenderTest {
    public static void main(String[] args) {
        new WorldRenderTest();
    }

    private ClientWorld world = new ClientWorld();
    private ChunkRenderer chunkRenderer = new ChunkRenderer();

    private GameApplet applet;
    private UIRootContainer rootContainer;
    private boolean running;

    private WorldRenderTest() {
        this.applet = new GameApplet(true);
        this.running = true;

        JFrame frame = new JFrame("Shovo");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                WorldRenderTest.this.running = false;
            }
        });

        frame.setLayout(new BorderLayout(0, 0));
        frame.add(applet, BorderLayout.CENTER);

        applet.setPreferredSize(new Dimension(1280, 720));
        frame.pack();

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        applet.init();
        applet.start();

        Chunk chunk = new Chunk(world, 0, 0, 0);
        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 16; y++) {
                chunk.setTile(x, y,  Tile.DIRT.getID());
            }
        }
        world.getChunkMap().put(chunk);

        rootContainer = new UIRootContainer(applet.getWidth(), applet.getHeight());
        rootContainer.addComponent(new WorldContainer(world, chunkRenderer));

        renderLoop();
    }

    private void renderLoop() {
        BufferStrategy buffer = applet.getCanvasBuffer();
        while(running) {
            rootContainer.update(-1);
            if(rootContainer.shouldRepaint()) {
                Graphics2D g = (Graphics2D)buffer.getDrawGraphics();
                rootContainer.draw(g);
                buffer.show();
            }
        }
    }

    class WorldContainer extends UIContainer {
        private ChunkRenderer chunkRenderer;
        private ClientWorld world;

        public WorldContainer(ClientWorld world, ChunkRenderer chunkRenderer) {
            super();
            this.chunkRenderer = chunkRenderer;
            this.world = world;

            super.width = 1280;
            super.height = 720;
            super.resizeBuffer(1280, 720);
        }

        @Override
        public void update(int delta) {
            super.setSize(parent.getWidth(), parent.getHeight());
            super.repaint();
        }

        @Override
        public void draw(Graphics2D g) {
            Chunk chunk = world.getChunkMap().get(0, 0, 0);
            chunkRenderer.renderChunk(chunk);
            chunkRenderer.draw(g);
        }

        public ChunkRenderer getChunkRenderer() {
            return chunkRenderer;
        }
    }
}
