package com.brokenpineapple.shovo.client;

import java.applet.Applet;
import java.awt.*;
import java.awt.image.BufferStrategy;

public class GameApplet extends Applet {
    private Canvas canvas;
    private BufferStrategy buffer;

    private boolean embedded;

    public GameApplet() throws HeadlessException {
        this(false);
    }

    public GameApplet(boolean embedded) throws HeadlessException {
        this.embedded = embedded;
    }

    @Override
    public void init() {
        super.setLayout(new BorderLayout(0, 0));

        this.canvas = new Canvas();
        super.add(canvas, BorderLayout.CENTER);

        canvas.createBufferStrategy(2);
        this.buffer = canvas.getBufferStrategy();

        if(!embedded) {
            Client.getInstance().launch(this);
        }
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public BufferStrategy getCanvasBuffer() {
        return buffer;
    }
}
