package com.brokenpineapple.shovo.client;

import com.brokenpineapple.shovo.client.gui.UIContainer;
import com.brokenpineapple.shovo.client.gui.UIRootContainer;
import com.brokenpineapple.shovo.client.gui.Views;
import com.brokenpineapple.shovo.client.net.NetManager;
import com.brokenpineapple.shovo.client.world.ClientWorld;
import com.brokenpineapple.shovo.core.Realm;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferStrategy;
import java.io.File;

public class Client {
    /* Singleton using eager initialization */
    private static final Client instance = new Client();

    public static Client getInstance() {
        return instance;
    }

    public static final File runPath = new File("").getParentFile();

    private GameApplet applet;
    private boolean inBrowser;

    private boolean running;

    private UIRootContainer rootContainer;

    private UIContainer view;

    private NetManager netManager = new NetManager();

    private ClientWorld world = new ClientWorld();

    private Client() {

    }

    // Launch from command line (in JFrame)
    @SuppressWarnings("UnusedParameters")
    public void launch(String[] args) {
        this.applet = new GameApplet(true);

        JFrame frame = new JFrame("Shovo");

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Client.this.running = false;
            }
        });

        frame.setLayout(new BorderLayout(0, 0));
        frame.add(applet, BorderLayout.CENTER);

        applet.setPreferredSize(new Dimension(1280, 720));
        frame.pack();

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        applet.init();
        applet.start();

        this.launch();
    }

    // Launch from browser (in JApplet)
    public void launch(GameApplet applet) {
        this.applet = applet;
        this.inBrowser = true;

        this.launch();
    }

    private void launch() {
        this.createListeners();

        view = Views.uiMainMenu;
        rootContainer = new UIRootContainer(applet.getWidth(), applet.getHeight());
        rootContainer.addComponent(view);

        this.running = true;
        this.gameLoop();
    }

    private void createListeners() {
        Canvas canvas = applet.getCanvas();

        canvas.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                rootContainer.onKeyDown(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {
                rootContainer.onKeyUp(e);
            }
        });

        canvas.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) { }

            @Override
            public void mousePressed(MouseEvent e) {
                rootContainer.onMouseDown(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                rootContainer.onMouseUp(e);
            }

            @Override
            public void mouseEntered(MouseEvent e) { }

            @Override
            public void mouseExited(MouseEvent e) { }
        });

        canvas.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                rootContainer.onMouseDrag(e);
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                rootContainer.onMouseMove(e);
            }
        });
    }

    private void gameLoop() {
        BufferStrategy buffer = applet.getCanvasBuffer();
        while(running) {
            rootContainer.update(-1);
            if(rootContainer.shouldRepaint()) {
                Graphics2D g = (Graphics2D) buffer.getDrawGraphics();
                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                rootContainer.draw(g);
                buffer.show();
            }
        }
    }

    public static void main(final String[] args) throws Exception {
        Realm.registerClient();

        Client client = Client.instance;
        client.launch(args);
    }

    public NetManager getNetManager() {
        return netManager;
    }

    public boolean isInBrowser() {
        return inBrowser;
    }

    public UIContainer getView(UIContainer view) {
        return view;
    }

    public void setView(UIContainer view) {
        this.view = view;
    }

    public File getRunPath() {
        return runPath;
    }

    public ClientWorld getWorld() {
        return world;
    }
}
