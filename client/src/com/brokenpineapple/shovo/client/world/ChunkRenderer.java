package com.brokenpineapple.shovo.client.world;

import com.brokenpineapple.shovo.core.world.Chunk;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

public class ChunkRenderer {
    private static final int TILE_SIZE = 16;

    private Map<Chunk, BufferedImage> backgroundCache = new HashMap<Chunk, BufferedImage>();
    private Map<Chunk, BufferedImage> foregroundCache = new HashMap<Chunk, BufferedImage>();

    public void renderChunk(Chunk chunk) {
        BufferedImage backgroundBuffer = backgroundCache.get(chunk);
        if(backgroundBuffer == null) {
            backgroundBuffer = new BufferedImage(Chunk.CHUNK_SIZE * TILE_SIZE, Chunk.CHUNK_SIZE * TILE_SIZE, BufferedImage.TYPE_INT_ARGB);
            backgroundCache.put(chunk, backgroundBuffer);
        }

        BufferedImage foregroundBuffer = backgroundCache.get(chunk);
        if(foregroundBuffer == null) {
            foregroundBuffer = new BufferedImage(Chunk.CHUNK_SIZE * TILE_SIZE, Chunk.CHUNK_SIZE * TILE_SIZE, BufferedImage.TYPE_INT_ARGB);
            foregroundCache.put(chunk, foregroundBuffer);
        }

        Graphics bg = backgroundBuffer.getGraphics();
        Graphics fg = foregroundBuffer.getGraphics();

        //bg.translate(chunk.getX() * Chunk.CHUNK_SIZE * TILE_SIZE, chunk.getY() * Chunk.CHUNK_SIZE * TILE_SIZE);
        //fg.translate(chunk.getX() * Chunk.CHUNK_SIZE * TILE_SIZE, chunk.getY() * Chunk.CHUNK_SIZE * TILE_SIZE);

        chunk.render(bg, fg);

        bg.dispose();
        fg.dispose();
    }

    public void draw(Graphics g) {
        for (Map.Entry<Chunk, BufferedImage> cacheEntry : backgroundCache.entrySet()) {
            BufferedImage img = cacheEntry.getValue();
            Chunk chunk = cacheEntry.getKey();
            g.drawImage(img, chunk.getX() * Chunk.CHUNK_SIZE * TILE_SIZE, chunk.getY() * Chunk.CHUNK_SIZE * TILE_SIZE, Chunk.CHUNK_SIZE * TILE_SIZE, Chunk.CHUNK_SIZE * TILE_SIZE, null);
        }

        for (Map.Entry<Chunk, BufferedImage> cacheEntry : foregroundCache.entrySet()) {
            BufferedImage img = cacheEntry.getValue();
            Chunk chunk = cacheEntry.getKey();
            g.drawImage(img, chunk.getX() * 16, chunk.getY() * 16, 256, 256, null);
        }
    }

    public void unloadChunk(Chunk chunk) {
        backgroundCache.remove(chunk);
        foregroundCache.remove(chunk);
    }

}
