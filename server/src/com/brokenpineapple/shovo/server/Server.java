package com.brokenpineapple.shovo.server;

import com.brokenpineapple.shovo.core.Constants;
import com.brokenpineapple.shovo.core.Realm;
import com.brokenpineapple.shovo.core.world.World;
import com.brokenpineapple.shovo.server.net.NetManager;
import com.brokenpineapple.shovo.server.world.ServerWorld;
import com.brokenpineapple.shovo.server.world.WorldIO;
import com.brokenpineapple.shovo.server.world.generators.FlatGrassGenerator;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

public class Server {
    private static Server instance;

    public static void main(String[] args) {
        Realm.registerServer();
        Path worldPath;
        int port;
        if(args.length > 0) {
            worldPath = runPath.getParent().resolve(args[0]);
            if(args.length == 2) {
                port = Integer.parseInt(args[1]);
            } else {
                port = Constants.DEFAULT_PORT;
            }
        } else {
            System.err.println("Usage: Server <world> [port]");
            return;
        }

        new Server(worldPath, port);
    }

    public static Server getInstance() {
        return instance;
    }

    private NetManager netManager;
    private ServerWorld world;
    private WorldIO worldIO;
    public static final Path runPath = FileSystems.getDefault().getPath(new File("").getAbsolutePath()).getParent();

    private Server(Path worldPath, int port) {
        instance = this;

        netManager = new NetManager(port);
        world = new ServerWorld(new FlatGrassGenerator());
        worldIO = new WorldIO(worldPath);

        this.init();
    }

    private void init() {
        if (!worldIO.chunkExists(0, 0, 0)) {
            generateCenter();
        }

        setupNetworking();
    }

    private void generateCenter() {
        for (int x = -7; x < 8; x++) {
            for (int y = -7; y < 8; y++) {
                world.generate(x, y, 0);
            }
        }
    }

    private void setupNetworking() {
        try {
            netManager.open();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public NetManager getNetManager() {
        return netManager;
    }

    public WorldIO getWorldIO() {
        return worldIO;
    }

    public World getWorld() {
        return world;
    }
}
