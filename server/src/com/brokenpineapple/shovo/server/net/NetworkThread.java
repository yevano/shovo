package com.brokenpineapple.shovo.server.net;

import com.brokenpineapple.shovo.core.net.Dispatcher;
import com.brokenpineapple.shovo.core.net.InputQueue;
import com.brokenpineapple.shovo.core.net.Packet;
import com.brokenpineapple.shovo.core.world.entities.PlayerEntity;
import com.brokenpineapple.shovo.server.Players;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class NetworkThread implements Runnable {
    private Selector selector;

    private Map<SocketChannel, ConnectionData> dataMap = new HashMap<SocketChannel, ConnectionData>();
    private boolean running = false;

    public NetworkThread(Selector selector) {
        this.selector = selector;
    }

    public void start() {
        this.running = true;
        new Thread(this).start();
    }

    public void stop() {
        this.running = false;
    }

    @Override
    public void run() {
        try {
            while (this.selector.select() > 0 && running) {
                try {
                    Set keys = this.selector.selectedKeys();
                    Iterator iterator = keys.iterator();

                    while (iterator.hasNext()) {
                        SelectionKey key = (SelectionKey) iterator.next();

                        if (key.isAcceptable()) {
                            this.accept(key);
                        } else if (key.isReadable()) {
                            this.read(key);
                        }

                        iterator.remove();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void accept(SelectionKey key) throws IOException {
        ServerSocketChannel serverChannel = (ServerSocketChannel) key.channel();
        SocketChannel clientChannel = serverChannel.accept();
        Players.register(clientChannel, new PlayerEntity());
        System.out.println("Accepted a new connection!");
        clientChannel.configureBlocking(false);
        clientChannel.register(this.selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
        dataMap.put(clientChannel, new ConnectionData());
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel clientChannel = (SocketChannel) key.channel();
        try {
            ConnectionData data = dataMap.get(clientChannel);
            InputQueue inputQueue = data.inputQueue;

            // Read the bytes into the inputQueue
            int len = inputQueue.fillFrom(clientChannel);
            System.out.println("Read " + len + " bytes");

            if (inputQueue.position() >= Packet.HEADER_BYTES && data.currentPayloadSize == -1) {
                System.out.println("Storing packet header info...");
                // If we have read the packet header, store it and remove it from the queue.
                ByteBuffer packetHeader = inputQueue.peek(Packet.HEADER_BYTES);

                data.currentID = packetHeader.getShort() & 0xffff;
                data.currentPayloadSize = packetHeader.getInt();
                inputQueue.dequeBytes(Packet.HEADER_BYTES);
            }

            System.out.println("Checking payload: payloadSize = " + data.currentPayloadSize + ", read = " + inputQueue.position());
            if (data.currentPayloadSize <= inputQueue.position() && data.currentPayloadSize != -1) {
                // If the entire payload has been read, send the packet to the Dispatcher
                ByteBuffer packetPayload = inputQueue.peek(data.currentPayloadSize);
                inputQueue.dequeBytes(data.currentPayloadSize);

                Dispatcher.getInstance().dispatch(data.currentID, packetPayload.array(), clientChannel);
                data.currentPayloadSize = -1;
            }
        } catch(IOException e) {
            e.printStackTrace();
            dataMap.remove(clientChannel);
        }
    }

    private class ConnectionData {
        public InputQueue inputQueue = new InputQueue();
        public int currentID;
        public int currentPayloadSize = -1;
    }
}
