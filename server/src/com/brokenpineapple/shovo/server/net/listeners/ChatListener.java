package com.brokenpineapple.shovo.server.net.listeners;

import com.brokenpineapple.shovo.core.net.packets.client.LocalChatPacket;
import com.brokenpineapple.shovo.core.net.InputListener;
import com.brokenpineapple.shovo.core.net.InputHandler;

public class ChatListener extends InputListener {
    @InputHandler(LocalChatPacket.class)
    public void onLocalChat(LocalChatPacket packet) {
        System.out.println(packet.getSender().socket().getInetAddress() + " sent: " + packet.getMessage());
    }
}
