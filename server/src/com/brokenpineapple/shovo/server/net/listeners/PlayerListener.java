package com.brokenpineapple.shovo.server.net.listeners;

import com.brokenpineapple.shovo.core.net.InputHandler;
import com.brokenpineapple.shovo.core.net.InputListener;
import com.brokenpineapple.shovo.core.net.packets.client.ChunkRequestPacket;
import com.brokenpineapple.shovo.core.net.packets.client.MovePacket;
import com.brokenpineapple.shovo.core.world.entities.PlayerEntity;
import com.brokenpineapple.shovo.server.Players;

public class PlayerListener extends InputListener {
    @InputHandler(MovePacket.class)
    public void onMove(MovePacket packet) {
        PlayerEntity player = Players.get(packet.getSender());
        player.setX(packet.getX());
        player.setY(packet.getY());
    }

    @InputHandler(ChunkRequestPacket.class)
    public void onChunkRequest(ChunkRequestPacket packet) {

    }
}
