package com.brokenpineapple.shovo.server.net;

import com.brokenpineapple.shovo.core.net.Packet;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class NetManager {

    private ServerSocketChannel serverChannel;
    private Selector selector;
    private NetworkThread networkThread;
    private int port;

    public NetManager(int port) {
        this.port = port;
    }

    public void open() throws IOException {
        serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);
        serverChannel.socket().bind(new InetSocketAddress(port));

        selector = Selector.open();
        serverChannel.register(selector, SelectionKey.OP_ACCEPT);

        networkThread = new NetworkThread(selector);
        networkThread.start();
    }

    public void sendPacket(Packet packet, SocketChannel clientChannel) throws IOException {
        ByteArrayOutputStream payloadStream = new ByteArrayOutputStream();
        packet.write(new DataOutputStream(payloadStream));
        byte[] payload = payloadStream.toByteArray();

        ByteArrayOutputStream headerByteStream = new ByteArrayOutputStream();
        DataOutputStream headerStream = new DataOutputStream(headerByteStream);
        headerStream.writeShort(packet.getID());
        headerStream.writeInt(payload.length);

        clientChannel.write(ByteBuffer.wrap(headerByteStream.toByteArray()));
        clientChannel.write(ByteBuffer.wrap(payload));
    }
}
