package com.brokenpineapple.shovo.server.world;

import com.brokenpineapple.shovo.core.world.Chunk;
import com.brokenpineapple.shovo.core.world.World;

public class ServerWorld extends World {
    ChunkGenerator chunkGenerator;

    public ServerWorld(ChunkGenerator chunkGenerator) {
        this.chunkGenerator = chunkGenerator;
    }

    public void generate(int x, int y, int z) {
        int[] tiles = chunkGenerator.generateTiles(x, y);
        Chunk chunk = new Chunk(this, x, y, z);
        for (int i = 0; i < tiles.length; i++) {
            int tileX = i % Chunk.CHUNK_SIZE;
            int tileY = i / Chunk.CHUNK_SIZE;
            chunk.setTile(tileX, tileY, tiles[i]);
        }
        super.getChunkMap().put(chunk);
    }
}
