package com.brokenpineapple.shovo.server.world;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class WorldIO {
    public Path worldPath;

    public WorldIO(Path worldPath) {
        this.worldPath = worldPath;
    }

    public byte[] readChunk(int x, int y, int z) throws IOException {
        return Files.readAllBytes(getChunkPath(x, y, z));
    }

    public void writeChunk(byte[] chunkBytes, int x, int y, int z) throws IOException {
        Files.write(getChunkPath(x, y, z), chunkBytes);
    }

    public Path getChunkPath(int x, int y, int z) {
        return worldPath.resolve(x + "-" + y + "-" + z + ".chunk");
    }

    public boolean chunkExists(int x, int y, int z) {
        return Files.exists(getChunkPath(x, y, z));
    }
}
