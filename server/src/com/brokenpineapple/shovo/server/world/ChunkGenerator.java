package com.brokenpineapple.shovo.server.world;

public interface ChunkGenerator {
    public int[] generateTiles(int x, int y);
}
