package com.brokenpineapple.shovo.server.world.generators;

import com.brokenpineapple.shovo.core.world.Chunk;
import com.brokenpineapple.shovo.core.world.Tile;
import com.brokenpineapple.shovo.server.world.*;

public class FlatGrassGenerator implements ChunkGenerator {
    @Override
    public int[] generateTiles(int x, int y) {
        int[] chunk = new int[Chunk.TILE_COUNT];
        for (int i = 0; i < chunk.length; i++) {
            chunk[i] = Tile.DIRT.getID();
        }
        return chunk;
    }
}
