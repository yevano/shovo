package com.brokenpineapple.shovo.server.testbed;

import com.brokenpineapple.shovo.core.net.Dispatcher;
import com.brokenpineapple.shovo.core.net.InputHandler;
import com.brokenpineapple.shovo.core.net.InputListener;
import com.brokenpineapple.shovo.core.net.packets.client.AuthPacket;
import com.brokenpineapple.shovo.core.net.packets.client.LocalChatPacket;
import com.brokenpineapple.shovo.core.net.packets.server.AuthResponsePacket;
import com.brokenpineapple.shovo.server.net.NetManager;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

public class ServerNetTest extends InputListener {
    public static void main(String[] args) throws Exception {
        new ServerNetTest();
    }

    private NetManager netManager;

    private ServerNetTest() throws IOException {
        Path path = FileSystems.getDefault().getPath(new File("").getAbsolutePath()).getParent();
        System.out.println(path);

        this.netManager = new NetManager(33433);
        this.netManager.open();
        System.out.println("Opened server socket");
        Dispatcher.getInstance().registerListener(this);
    }

    @InputHandler(AuthPacket.class)
    public void onAuth(AuthPacket packet) throws IOException {
        System.out.println("AuthPacket received!");
        System.out.println("Username: " + packet.getUsername());
        System.out.println("Password: " + packet.getPassword());
        netManager.sendPacket(new AuthResponsePacket(true), packet.getSender());
    }
}
