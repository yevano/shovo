package com.brokenpineapple.shovo.server;

import com.brokenpineapple.shovo.core.world.entities.PlayerEntity;

import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Map;

public class Players {
    private static final Map<SocketChannel, PlayerEntity> playerMap = new HashMap<SocketChannel, PlayerEntity>();

    public static void register(SocketChannel clientChannel, PlayerEntity player) {
        playerMap.put(clientChannel, player);
    }

    public static PlayerEntity get(SocketChannel channel) {
        return playerMap.get(channel);
    }
}
