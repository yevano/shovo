package com.brokenpineapple.shovo.core;

public interface Future<T> {
    public void future(T value);
}
