package com.brokenpineapple.shovo.core.world;

import java.awt.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Chunk {
    public static final int CHUNK_SIZE = 16;
    public static final int TILE_COUNT = CHUNK_SIZE * CHUNK_SIZE;

    private final World world;
    private int x, y, z;
    private int rx, ry;

    private final short[] tiles = new short[TILE_COUNT];
    private final Object[] metadata = new Object[TILE_COUNT];

    private List<Entity> entities = new ArrayList<Entity>();

    public Chunk(World world, int x, int y, int z) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.rx = x * CHUNK_SIZE;
        this.ry = y * CHUNK_SIZE;
    }

    public void render(Graphics bg, Graphics fg) {
        for(int x = 0; x < CHUNK_SIZE; x++) {
            for(int y = 0; y < CHUNK_SIZE; y++) {
                bg.translate(x * 16, y * 16);
                fg.translate(x * 16, y * 16);
                Tile.get(tiles[y * CHUNK_SIZE + x]).draw(world, bg, fg, metadata[y * CHUNK_SIZE + x], rx + x, ry + y, z);
                bg.translate(-x * 16, -y * 16);
                fg.translate(-x * 16, -y * 16);
            }
        }
    }

    public void onLoad() {
        world.addEntities(entities);
    }

    public void onUnload() {
        world.removeEntities(entities);
    }

    public int getTile(int x, int y) {
        return tiles[y * CHUNK_SIZE + x] & 0xffff;
    }

    public void setTile(int x, int y, int tile) {
        this.tiles[y * CHUNK_SIZE + x] = (short)tile;
    }

    public Object getMetadata(int x, int y) {
        return metadata[y * CHUNK_SIZE + x];
    }

    public void setMetadata(int x, int y, Object metadata) {
        this.metadata[y * CHUNK_SIZE + y] = metadata;
    }

    public void addEntity(Entity entity) {
        entities.add(entity);
    }

    public void removeEntity(Entity entity) {
        entities.remove(entity);
    }

    public void read(DataInputStream stream) throws IOException {
        this.x = stream.readInt();
        this.y = stream.readInt();
        this.z = stream.readInt();

        this.rx = x * CHUNK_SIZE;
        this.ry = y * CHUNK_SIZE;

        for(int i = 0; i < TILE_COUNT; i++) {
            short id = stream.readShort();
            Object metadata = Tile.get(id & 0xffff).readMetadata(stream);

            this.tiles[i] = id;
            this.metadata[i] = metadata;
        }

        int entityCount = stream.readUnsignedShort();
        for(int i = 0; i < entityCount; i++) {
            int id = stream.readUnsignedShort();
            Entity entity;
            try {
                entity = Entity.get(id).newInstance();
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
            entity.read(stream);
            entities.add(entity);
        }
    }

    public void write(DataOutputStream stream) throws IOException {
        stream.writeInt(x);
        stream.writeInt(y);
        stream.writeInt(z);

        for(int i = 0; i < TILE_COUNT; i++) {
            int id = this.tiles[i];
            Object metadata = this.metadata[i];

            stream.writeShort(id);
            Tile.get(id).writeMetadata(stream, metadata);
        }

        stream.write(entities.size());
        for(Entity entity : entities) {
            stream.writeShort(Entity.getID(entity.getClass()));
            entity.write(stream);
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }
}
