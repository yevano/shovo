package com.brokenpineapple.shovo.core.world;

import com.brokenpineapple.shovo.core.world.entities.PlayerEntity;

import java.awt.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public abstract class Entity {

    private static final Class<? extends Entity>[] entities = new Class[] {
            PlayerEntity.class
    };

    public static Class<? extends Entity> get(int id) {
        return entities[id];
    }

    public static int getID(Class<? extends Entity> entity) {
        for(int id = 0; id < entities.length; id++) {
            if(entities[id] == entity) {
                return id;
            }
        }
        return -1;
    }

    private int id;
    private float x, y;
    private int z;

    public void init() {

    }

    public void update(int delta) {

    }

    public void draw(World world, Graphics g) {

    }

    public void read(DataInputStream stream) throws IOException {
        this.x = stream.readFloat();
        this.y = stream.readFloat();
        this.z = stream.readInt();

    }

    public void write(DataOutputStream stream) throws IOException {
        stream.writeFloat(x);
        stream.writeFloat(y);
        stream.writeInt(z);
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public int getId() {
        return id;
    }
}
