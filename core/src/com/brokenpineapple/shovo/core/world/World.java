package com.brokenpineapple.shovo.core.world;

import java.util.ArrayList;
import java.util.List;

public class World {
    private ChunkMap chunkMap = new ChunkMap();

    private List<Entity> entities = new ArrayList<Entity>();

    public void addEntities(List<Entity> entities) {
        this.entities.addAll(entities);
    }

    public void removeEntities(List<Entity> entities) {
        this.entities.removeAll(entities);
    }

    public int getTile(int x, int y, int z) {
        Chunk chunk = chunkMap.get(x / 16, y / 16, z);
        return chunk.getTile(x % 16, y % 16);
    }

    public void setTile(int x, int y, int z, int tile) {
        Chunk chunk = chunkMap.get(x / 16, y / 16, z);
        chunk.setTile(x % 16, y % 16, tile);
    }

    public Object getMetadata(int x, int y, int z) {
        Chunk chunk = chunkMap.get(x / 16, y / 16, z);
        return chunk.getMetadata(x % 16, y % 16);
    }

    public void setMetadata(int x, int y, int z, Object metadata) {
        Chunk chunk = chunkMap.get(x / 16, y / 16, z);
        chunk.setMetadata(x % 16, y % 16, metadata);
    }

    public ChunkMap getChunkMap() {
        return chunkMap;
    }
}
