package com.brokenpineapple.shovo.core.world;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public abstract class ChunkUpdate {
    public abstract void read(DataInputStream stream) throws IOException;
    public abstract void write(DataOutputStream stream) throws IOException;

    public int getID() {
        return ChunkUpdateType.getByClass(this.getClass()).getID();
    }
}
