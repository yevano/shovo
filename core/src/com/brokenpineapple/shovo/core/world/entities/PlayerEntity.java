package com.brokenpineapple.shovo.core.world.entities;

import com.brokenpineapple.shovo.core.world.Entity;
import com.brokenpineapple.shovo.core.world.World;

import java.awt.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PlayerEntity extends Entity {

    @Override
    public void init() {
        super.init();
    }

    @Override
    public void update(int delta) {
        super.update(delta);
    }

    @Override
    public void draw(World world, Graphics g) {

    }

    @Override
    public void read(DataInputStream stream) throws IOException {
        super.read(stream);
    }

    @Override
    public void write(DataOutputStream stream) throws IOException {
        super.write(stream);
    }
}
