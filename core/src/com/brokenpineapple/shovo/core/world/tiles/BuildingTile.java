package com.brokenpineapple.shovo.core.world.tiles;

import com.brokenpineapple.shovo.core.world.Entity;
import com.brokenpineapple.shovo.core.world.Tile;
import com.brokenpineapple.shovo.core.world.World;

import java.awt.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class BuildingTile extends Tile {
    class BuildingMetadata {
        int floorColorPrimary;
        int floorColorSecondary;

        boolean hasWalls;
        int wallColor;
        byte wallData;

        boolean hasDoor;
        int doorColor;
        byte doorMap;
        byte doorState;
    }

    public BuildingTile(int id) {
        super(id);
    }

    @Override
    public byte[] getCollisionMap() {
        return new byte[0];
    }

    @Override
    public void init() {
    }

    @Override
    public void draw(World world, Graphics bg, Graphics fg, Object metadata, int x, int y, int z) {
    }

    @Override
    public boolean hasForeground(World world, int x, int y, int z) {
        return false;
    }

    @Override
    public float getForegroundOpacity() {
        return 0;
    }

    @Override
    public boolean onCollide(World world, int x, int y, int z, int index, int value, Entity entity, int entityValue) {
        return false;
    }

    @Override
    public Object readMetadata(DataInputStream stream) throws IOException {
        return null;
    }

    @Override
    public void writeMetadata(DataOutputStream stream, Object metadata) throws IOException {
    }
}
