package com.brokenpineapple.shovo.core.world.tiles;

import com.brokenpineapple.shovo.core.world.Entity;
import com.brokenpineapple.shovo.core.world.Tile;
import com.brokenpineapple.shovo.core.world.World;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class DirtTile extends Tile {
    private BufferedImage texture = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

    public DirtTile(int id) {
        super(id);
    }

    @Override
    public byte[] getCollisionMap() {
        return new byte[0];
    }

    @Override
    public void init() {
        Graphics g = texture.getGraphics();
        g.setColor(new Color(0x0000FF00));
        g.fillRect(0, 0, 16, 16);
        g.dispose();
    }

    @Override
    public void draw(World world, Graphics bg, Graphics fg, Object metadata, int x, int y, int z) {
        bg.setColor(new Color(126, 53, 9));
        bg.fillRect(0, 0, 16, 16);
        bg.setColor(new Color(37, 16, 4));
        bg.drawRect(0, 0, 15, 15);
    }

    @Override
    public boolean hasForeground(World world, int x, int y, int z) {
        return false;
    }

    @Override
    public float getForegroundOpacity() {
        return 0;
    }

    @Override
    public boolean onCollide(World world, int x, int y, int z, int index, int value, Entity entity, int entityValue) {
        return false;
    }

    @Override
    public Object readMetadata(DataInputStream stream) throws IOException {
        DirtMetadata dirtMetadata = new DirtMetadata();
        dirtMetadata.vegetation = stream.readByte();
        return dirtMetadata;
    }

    @Override
    public void writeMetadata(DataOutputStream stream, Object metadata) throws IOException {
        DirtMetadata dirtMetadata = (DirtMetadata) metadata;
        stream.writeByte(dirtMetadata.vegetation);
    }

    class DirtMetadata {
        public byte vegetation;
    }
}
