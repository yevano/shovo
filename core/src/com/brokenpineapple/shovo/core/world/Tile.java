package com.brokenpineapple.shovo.core.world;

import com.brokenpineapple.shovo.core.world.tiles.DirtTile;

import java.awt.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public abstract class Tile {
    private static final Tile[] tiles = new Tile[(int) Short.MAX_VALUE * 2];

    public static Tile get(int id) {
        return tiles[id];
    }

    public static final DirtTile DIRT = new DirtTile(0);

    public final int id;

    protected Tile(int id) {
        this.id = id;
        if (tiles[id] != null) {
            throw new RuntimeException("Tried to register tile id " + id + " which is already taken.");
        }
        tiles[id] = this;
    }

    public abstract byte[] getCollisionMap();

    public abstract void init();

    public abstract void draw(World world, Graphics bg, Graphics fg, Object metadata, int x, int y, int z);

    public abstract boolean hasForeground(World world, int x, int y, int z);

    public abstract float getForegroundOpacity();

    public abstract boolean onCollide(World world, int x, int y, int z, int index, int value, Entity entity, int entityValue);

    public abstract Object readMetadata(DataInputStream stream) throws IOException;

    public abstract void writeMetadata(DataOutputStream stream, Object metadata) throws IOException;

    public int getID() {
        return id;
    }
}
