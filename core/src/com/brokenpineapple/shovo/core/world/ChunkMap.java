package com.brokenpineapple.shovo.core.world;

public class ChunkMap {
    class ChunkEntry {
        long xy;
        Chunk value;

        ChunkEntry next;
    }

    class LevelEntry {
        int z;
        ChunkEntry chunks;

        LevelEntry next;
    }

    private LevelEntry levels;

    public ChunkMap() {
        levels = new LevelEntry();
        levels.z = 0;
        levels.chunks = new ChunkEntry();
    }

    public Chunk get(int x, int y, int z) {
        long xy = ((long)x & 0xffffffff) | ((long)y & 0xffffffff) << 32;

        for(LevelEntry level = levels; level != null; level = level.next) {
            if(level.z == z) {
                for(ChunkEntry chunk = level.chunks; chunk != null; chunk = chunk.next) {
                    if(chunk.xy == xy) {
                        return chunk.value;
                    }
                }
                break;
            }
        }
        return null;
    }

    public void remove(Chunk chunk) {
        this.remove(chunk.getX(), chunk.getY(), chunk.getZ());
    }

    public void remove(int x, int y, int z) {
        long xy = ((long)x & 0xffffffff) | ((long)y & 0xffffffff) << 32;

        LevelEntry previousLevel = null;
        for(LevelEntry level = levels; level.next != null; level = level.next) {
            if(level.z == z) {
                ChunkEntry previousChunk = null;
                for(ChunkEntry chunk = level.chunks; chunk.next != null; chunk = chunk.next) {
                    if(chunk.xy == xy) {
                        if(previousChunk != null) {
                            previousChunk.next = chunk.next;
                        } else if(chunk.next != null) {
                            level.chunks = chunk.next;
                        } else if(previousLevel != null) {
                            previousLevel.next = level.next;
                        }
                        return;
                    }
                    previousChunk = chunk;
                }
                break;
            }
            previousLevel = level;
        }
    }

    public void put(Chunk chunk) {
        int z = chunk.getZ();
        System.out.flush();
        long xy = ((long)chunk.getX() & 0xffffffff) | ((long)chunk.getY() & 0xffffffff) << 32;
        LevelEntry level;

        for(level = levels; level != null; level = level.next) {
            System.out.flush();
            if(level.z == z) {
                ChunkEntry entry;

                for(entry = level.chunks; entry != null; entry = entry.next) {
                    if(entry.xy == xy) {
                        System.out.flush();
                        entry.value = chunk;
                        return;
                    }
                }

                entry.next = new ChunkEntry();
                entry.next.xy = xy;
                entry.next.value = chunk;
                return;
            }
        }

        level.next = new LevelEntry();
        level.next.z = z;

        ChunkEntry entry = new ChunkEntry();
        entry.next = new ChunkEntry();
        entry.next.xy = xy;
        entry.next.value = chunk;

        level.next.chunks = entry;
    }
}
