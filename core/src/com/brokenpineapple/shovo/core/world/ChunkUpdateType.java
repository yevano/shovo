package com.brokenpineapple.shovo.core.world;

import com.brokenpineapple.shovo.core.world.updates.BlockPlaceUpdate;

import java.util.HashMap;
import java.util.Map;

public enum ChunkUpdateType {

    BLOCK_PLACE(0x0, BlockPlaceUpdate.class);

    private static final Map<Integer, ChunkUpdateType> updateIDMap = new HashMap<Integer, ChunkUpdateType>();
    private static final Map<Class<? extends ChunkUpdate>, ChunkUpdateType> updateClassMap = new HashMap<Class<? extends ChunkUpdate>, ChunkUpdateType>();

    static {
        for(ChunkUpdateType type : values()) {
            updateIDMap.put(type.id, type);
            updateClassMap.put(type.updateClass, type);
        }
    }

    public static ChunkUpdateType getByID(int id) {
        return updateIDMap.get(id);
    }

    public static ChunkUpdateType getByClass(Class<? extends ChunkUpdate> updateClass) {
        return updateClassMap.get(updateClass);
    }

    int id;
    Class<? extends ChunkUpdate> updateClass;

    private ChunkUpdateType(int id, Class<? extends ChunkUpdate> updateClass) {
        this.id = id;
        this.updateClass = updateClass;
    }

    public int getID() {
        return id;
    }

    public Class<? extends ChunkUpdate> getUpdateClass() {
        return updateClass;
    }
}
