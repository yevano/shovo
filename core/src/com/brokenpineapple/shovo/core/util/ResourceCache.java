package com.brokenpineapple.shovo.core.util;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ResourceCache {
    private static final ResourceCache instance = new ResourceCache();

    private Map<File, BufferedImage> imageCache = new HashMap<File, BufferedImage>();

    private ResourceCache() { }

    public BufferedImage loadImage(File path) throws IOException {
        BufferedImage image;

        if (imageCache.containsKey(path))
            image = imageCache.get(path);
        else
            image = ImageIO.read(path);

        return image;
    }

    public static ResourceCache getInstance() {
        return instance;
    }
}
