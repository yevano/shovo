package com.brokenpineapple.shovo.core.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;

public class StreamFactory {
    public static DataInputStream newDataByteInputStream(int size) {
        return new DataInputStream(new ByteArrayInputStream(new byte[size]));
    }

    public static DataInputStream newDataByteInputStream(byte[] bytes) {
        return new DataInputStream(new ByteArrayInputStream(bytes));
    }

    public static DataOutputStream newDataByteOutputStream() {
        return new DataOutputStream(new ByteArrayOutputStream());
    }
}
