package com.brokenpineapple.shovo.core.net;

import com.brokenpineapple.shovo.core.net.packets.client.AuthPacket;
import com.brokenpineapple.shovo.core.net.packets.client.ChunkRequestPacket;
import com.brokenpineapple.shovo.core.net.packets.client.LocalChatPacket;
import com.brokenpineapple.shovo.core.net.packets.server.AuthResponsePacket;
import com.brokenpineapple.shovo.core.net.packets.server.ChatPacket;
import com.brokenpineapple.shovo.core.net.packets.server.ChunkPacket;
import com.brokenpineapple.shovo.core.net.packets.server.ErrorPacket;

import java.util.HashMap;
import java.util.Map;

public enum PacketType {
    // Client packets
    CHUNK_REQUEST(Side.CLIENT, 0x0, ChunkRequestPacket.class),
    LOCAL_CHAT(Side.CLIENT, 0x1, LocalChatPacket.class),
    AUTH(Side.CLIENT, 0x2, AuthPacket.class),

    // Server packets
    CHAT(Side.SERVER, 0x8000, ChatPacket.class),
    CHUNK(Side.SERVER, 0x8001, ChunkPacket.class),
    ERROR(Side.SERVER, 0x8002, ErrorPacket.class),
    AUTH_RESPONSE(Side.SERVER, 0x8003, AuthResponsePacket.class);

    public enum Side {
        CLIENT, SERVER, SHARED
    }

    private static final Map<Integer, PacketType> packetIDMap = new HashMap<Integer, PacketType>();
    private static final Map<Class<? extends Packet>, PacketType> packetClassMap = new HashMap<Class<? extends Packet>, PacketType>();

    static {
        for(PacketType type : values()) {
            packetIDMap.put(type.id, type);
            packetClassMap.put(type.packetClass, type);
        }
    }

    public static PacketType getByID(int id) {
        return packetIDMap.get(id);
    }

    public static PacketType getByClass(Class<? extends Packet> packetClass) {
        return packetClassMap.get(packetClass);
    }

    private Side side;
    private int id;
    private Class<? extends Packet> packetClass;

    private PacketType(Side side, int id, Class<? extends Packet> packetClass) {
        this.side = side;
        this.id = id;
        this.packetClass = packetClass;
    }

    public Side getSide() {
        return side;
    }

    public int getID() {
        return id;
    }

    public Class<? extends Packet> getPacketClass() {
        return packetClass;
    }
}