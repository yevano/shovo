package com.brokenpineapple.shovo.core.net;

import java.nio.ByteBuffer;

public class DynamicByteBuffer {
    public static final int DEFAULT_INITIAL_ALLOC = 1024;
    public static final int DEFAULT_READ_BUFFER_SIZE = 1024;
    public static final float DEFAULT_GROW_FACTOR = 1.5f;

    private ByteBuffer heapBuffer;
    private byte[] readBuffer;
    private float growFactor;

    public DynamicByteBuffer(int alloc, int readSize, float growFactor) {
        this.heapBuffer = ByteBuffer.allocate(alloc);
        this.readBuffer = new byte[readSize];
        this.growFactor = growFactor;
    }

    public DynamicByteBuffer() {
        this.heapBuffer = ByteBuffer.allocate(DEFAULT_INITIAL_ALLOC);
        this.readBuffer = new byte[DEFAULT_READ_BUFFER_SIZE];
        this.growFactor = DEFAULT_GROW_FACTOR;
    }

    public void resize(int alloc) {
        ByteBuffer newBuffer = ByteBuffer.allocate(alloc);
        int pos = heapBuffer.position();
        heapBuffer.position(0);
        newBuffer.put(heapBuffer);
        heapBuffer.position(pos);
        heapBuffer = newBuffer;
    }

    public boolean ensureCapacity(int minCapacity) {
        if (heapBuffer.remaining() < minCapacity) {
            this.grow(heapBuffer.capacity() + minCapacity);
            return false;
        }
        return true;
    }

    public void grow() {
        this.resize((int) (heapBuffer.capacity() * growFactor));
    }

    public void grow(int from) {
        this.resize((int) (from * growFactor));
    }

    public void insert(ByteBuffer insertion, int index) {
        int pos = heapBuffer.position();
        int len = insertion.remaining();
        ByteBuffer newBuffer;
        if(heapBuffer.remaining() >= len) {
            newBuffer = ByteBuffer.allocate(heapBuffer.capacity());
        } else {
            newBuffer = ByteBuffer.allocate(heapBuffer.position() + len);
        }

        heapBuffer.limit(index);
        heapBuffer.position(0);
        newBuffer.put(heapBuffer);
        newBuffer.put(insertion);
        heapBuffer.limit(pos);
        System.out.println("newBuffer.remaining = " + newBuffer.remaining() + "; heapBuffer.remaining = " + heapBuffer.remaining() + ";");
        newBuffer.put(heapBuffer);

        newBuffer.position(pos + len);
        heapBuffer = newBuffer;
    }

    // fromIndex inclusive, toIndex exclusive
    public void remove(int fromIndex, int toIndex) {
        int pos = heapBuffer.position();
        ByteBuffer newBuffer = ByteBuffer.allocate(heapBuffer.capacity());

        heapBuffer.limit(fromIndex);
        heapBuffer.position(0);
        newBuffer.put(heapBuffer);
        heapBuffer.limit(pos);
        heapBuffer.position(toIndex);
        newBuffer.put(heapBuffer);

        newBuffer.position(pos - (toIndex - fromIndex));
        heapBuffer = newBuffer;
    }

    public ByteBuffer sub(int fromIndex, int toIndex) {
        ByteBuffer returnBuffer = ByteBuffer.allocate(toIndex - fromIndex);
        int pos = heapBuffer.position();
        heapBuffer.position(fromIndex);
        heapBuffer.limit(toIndex);
        returnBuffer.put(heapBuffer);
        heapBuffer.limit(heapBuffer.capacity());
        heapBuffer.position(pos);
        returnBuffer.position(0);
        return returnBuffer;
    }

    public DynamicByteBuffer put(byte b) {
        this.ensureCapacity(1);
        this.heapBuffer.put(b);
        return this;
    }

    public DynamicByteBuffer put(byte[] src) {
        this.ensureCapacity(src.length);
        heapBuffer.put(src);
        return this;
    }

    public DynamicByteBuffer put(byte[] src, int offset, int length) {
        this.ensureCapacity(src.length);
        heapBuffer.put(src, offset, length);
        return this;
    }

    public DynamicByteBuffer put(ByteBuffer buffer) {
        this.ensureCapacity(buffer.remaining());
        heapBuffer.put(buffer);
        return this;
    }

    public DynamicByteBuffer putChar(char c) {
        this.ensureCapacity(2);
        heapBuffer.putChar(c);
        return this;
    }

    public DynamicByteBuffer putShort(short s) {
        this.ensureCapacity(2);
        heapBuffer.putShort(s);
        return this;
    }

    public DynamicByteBuffer putInt(int i) {
        this.ensureCapacity(4);
        heapBuffer.putInt(i);
        return this;
    }

    public DynamicByteBuffer putLong(long l) {
        this.ensureCapacity(8);
        heapBuffer.putLong(l);
        return this;
    }

    public byte get() {
        return heapBuffer.get();
    }

    public void get(byte[] dst) {
        heapBuffer.get(dst);
    }

    public void get(byte[] dst, int offset, int length) {
        heapBuffer.get(dst, offset, length);
    }

    public char getChar() {
        return heapBuffer.getChar();
    }

    public short getShort() {
        return heapBuffer.getShort();
    }

    public int getInt() {
        return heapBuffer.getInt();
    }

    public long getLong() {
        return heapBuffer.getLong();
    }

    public void mark() {
        heapBuffer.mark();
    }

    public void reset() {
        heapBuffer.reset();
    }

    public long position() {
        return heapBuffer.position();
    }

    public void position(int pos) {
        heapBuffer.position(pos);
    }
}
