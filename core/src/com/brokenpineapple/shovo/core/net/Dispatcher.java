package com.brokenpineapple.shovo.core.net;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.lang.reflect.Method;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Dispatcher {
    private static final Dispatcher instance = new Dispatcher();

    public static Dispatcher getInstance() {
        return instance;
    }

    private Map<Class<? extends Packet>, ListenerObject> handlerMap = new HashMap<Class<? extends Packet>, ListenerObject>();

    public void registerListener(InputListener listener) {
        for(Method method : listener.getClass().getMethods()) {
            InputHandler annotation;
            if ((annotation = method.getAnnotation(InputHandler.class)) != null) {
                if (!handlerMap.containsKey(annotation.value())) {
                    handlerMap.put(annotation.value(), new ListenerObject(listener));
                }

                handlerMap.get(annotation.value()).handlers.add(method);
            }
        }
    }

    public void dispatch(int id, byte[] payload, SocketChannel sender) {
        Class<? extends Packet> packetType = PacketType.getByID(id).getPacketClass();

        System.out.println(packetType);

        Packet packet = null;
        try {
            packet = packetType.getConstructor(new Class<?>[] {SocketChannel.class}).newInstance(sender);
            packet.read(new DataInputStream(new ByteArrayInputStream(payload)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        InputListener listener = handlerMap.get(packetType).listener;

        try {
            for (Method handler : handlerMap.get(packetType).handlers) {
                handler.invoke(listener, packet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class ListenerObject {
        public InputListener listener;
        public Set<Method> handlers;

        public ListenerObject(InputListener listener) {
            this.listener = listener;
            this.handlers = new HashSet<Method>();
        }
    }
}
