package com.brokenpineapple.shovo.core.net;

import com.brokenpineapple.shovo.core.net.Packet;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface InputHandler {
    public Class<? extends Packet> value();
}
