package com.brokenpineapple.shovo.core.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.channels.SocketChannel;

public abstract class Packet {
    public static final int HEADER_BYTES = 6;

    private SocketChannel sender;

    public Packet() { }

    public Packet(SocketChannel sender) {
        this.sender = sender;
    }

    public abstract void read(DataInputStream stream) throws IOException;
    public abstract void write(DataOutputStream stream) throws IOException;

    public SocketChannel getSender() {
        return sender;
    }

    public int getID() {
        return PacketType.getByClass(this.getClass()).getID();
    }
}
