package com.brokenpineapple.shovo.core.net.packets.server;

import com.brokenpineapple.shovo.core.net.Packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.channels.SocketChannel;

public class AuthResponsePacket extends Packet {
    private boolean successful;

    public AuthResponsePacket(SocketChannel sender) {
        super(sender);
    }

    public AuthResponsePacket(boolean successful) {
        this.successful = successful;
    }

    @Override
    public void read(DataInputStream stream) throws IOException {
        successful = stream.readBoolean();
    }

    @Override
    public void write(DataOutputStream stream) throws IOException {
        stream.writeBoolean(successful);
    }


    public boolean isSuccessful() {
        return successful;
    }
}
