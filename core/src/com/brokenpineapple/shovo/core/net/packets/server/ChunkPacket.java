package com.brokenpineapple.shovo.core.net.packets.server;

import com.brokenpineapple.shovo.core.net.Packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.channels.SocketChannel;

public class ChunkPacket extends Packet {
    private byte[] chunkData;

    private int x, y, z;

    public ChunkPacket(SocketChannel sender) {
        super(sender);
    }

    public ChunkPacket(byte[] chunkData, int x, int y, int z) {
        this.chunkData = chunkData;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void read(DataInputStream stream) throws IOException {
        this.chunkData = new byte[stream.readInt()];
        stream.read(chunkData);
        this.x = stream.readInt();
        this.y = stream.readInt();
        this.z = stream.readInt();
    }

    @Override
    public void write(DataOutputStream stream) throws IOException {
        stream.writeInt(chunkData.length);
        stream.write(chunkData);
        stream.writeInt(x);
        stream.writeInt(y);
        stream.writeInt(z);
    }

    public byte[] getChunk() {
        return chunkData;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }
}
