package com.brokenpineapple.shovo.core.net.packets.client;

import com.brokenpineapple.shovo.core.net.Packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.channels.SocketChannel;

public class LocalChatPacket extends Packet {
    private String message;

    public LocalChatPacket(SocketChannel sender) {
        super(sender);
    }

    public LocalChatPacket(String message) {
        this.message = message;
    }

    @Override
    public void read(DataInputStream stream) throws IOException {
        byte[] messageBuffer = new byte[stream.readShort()];
        stream.read(messageBuffer);
        message = new String(messageBuffer);
    }

    @Override
    public void write(DataOutputStream stream) throws IOException {
        stream.writeShort((short) message.length());
        stream.write(message.getBytes());
    }

    public String getMessage() {
        return message;
    }
}
