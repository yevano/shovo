package com.brokenpineapple.shovo.core.net.packets.server;

import com.brokenpineapple.shovo.core.net.Packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.channels.SocketChannel;

public class ErrorPacket extends Packet {
    private String message;

    public ErrorPacket(SocketChannel sender) {
        super(sender);
    }

    public ErrorPacket(String message) {
        this.message = message;
    }

    @Override
    public void read(DataInputStream stream) throws IOException {
        byte[] messageBytes = new byte[stream.readShort()];
        stream.read(messageBytes);
        message = new String(messageBytes);
    }

    @Override
    public void write(DataOutputStream buffer) throws IOException {
        buffer.writeShort((short) message.length());
        buffer.write(message.getBytes());
    }

    public String getMessage() {
        return message;
    }
}
