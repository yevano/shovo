package com.brokenpineapple.shovo.core.net.packets.client;

import com.brokenpineapple.shovo.core.net.Packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.channels.SocketChannel;

public class MovePacket extends Packet {
    private float x, y;

    public MovePacket(SocketChannel sender) {
        super(sender);
    }

    public MovePacket(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void read(DataInputStream stream) throws IOException {
        x = stream.readFloat();
        y = stream.readFloat();
    }

    @Override
    public void write(DataOutputStream stream) throws IOException {
        stream.writeFloat(x);
        stream.writeFloat(y);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
}
