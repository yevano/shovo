package com.brokenpineapple.shovo.core.net.packets.client;

import com.brokenpineapple.shovo.core.net.Packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.channels.SocketChannel;

public class AuthPacket extends Packet {
    private String username;
    private String password;

    public AuthPacket(SocketChannel sender) {
        super(sender);
    }

    public AuthPacket(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public void read(DataInputStream stream) throws IOException {
        byte[] usernameBuffer = new byte[stream.readByte()];
        stream.read(usernameBuffer);
        username = new String(usernameBuffer);

        byte[] passwordBuffer = new byte[stream.readByte()];
        stream.read(passwordBuffer);
        password = new String(passwordBuffer);
    }

    @Override
    public void write(DataOutputStream stream) throws IOException {
        stream.writeByte((byte) username.length());
        stream.write(username.getBytes());

        stream.writeByte((byte) password.length());
        stream.write(password.getBytes());
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
