package com.brokenpineapple.shovo.core.net.packets.server;

import com.brokenpineapple.shovo.core.net.Packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.channels.SocketChannel;

public class ChunkUpdatePacket extends Packet {
    private byte[][] chunkUpdates;

    public ChunkUpdatePacket(SocketChannel sender) {
        super(sender);
    }

    public ChunkUpdatePacket(byte[][] chunkUpdates) {
        this.chunkUpdates = chunkUpdates;
    }

    @Override
    public void read(DataInputStream stream) throws IOException {
        int updateAmount = stream.readInt();

        for (int i = 0; i < updateAmount; i++) {
            chunkUpdates[i] = new byte[stream.readInt()];
            stream.read(chunkUpdates[i]);
        }
    }

    @Override
    public void write(DataOutputStream stream) throws IOException {
        stream.writeInt(chunkUpdates.length);

        for (int i = 0; i < chunkUpdates.length; i++) {
            stream.writeInt(chunkUpdates[i].length);
            stream.write(chunkUpdates[i]);
        }
    }

    public byte[][] getChunkUpdates() {
        return chunkUpdates;
    }
}
