package com.brokenpineapple.shovo.core.net;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

public class InputQueue extends DynamicByteBuffer {
    private ByteBuffer channelReadBuffer = ByteBuffer.allocate(1024);

    public int fillFrom(ReadableByteChannel channel) throws IOException {
        int readLen;
        int totalLen = 0;
        while ((readLen = channel.read(channelReadBuffer)) != 0) {
            totalLen += readLen;
            channelReadBuffer.position(0);
            super.put(channelReadBuffer);
            channelReadBuffer.position(0);
        }
        return totalLen;
    }

    public void fillFrom(ReadableByteChannel channel, int readSize) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(readSize);
        channel.read(buffer);
        super.put(buffer);
    }

    public ByteBuffer peek(int amount) {
        return super.sub(0, amount);
    }

    public void dequeBytes(int count) {
        super.remove(0, count);
    }
}
