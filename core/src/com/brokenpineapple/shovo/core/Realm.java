package com.brokenpineapple.shovo.core;

public class Realm {
    private static boolean SERVER;

    public static void registerServer() {
        SERVER = true;
    }

    public static void registerClient() {
        SERVER = false;
    }

    public static boolean isServer() {
        return SERVER;
    }

    public static boolean isClient() {
        return !SERVER;
    }
}
